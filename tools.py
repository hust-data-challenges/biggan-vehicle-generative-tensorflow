import os

from utils import convert_png

missing_paths = [
    'cqtqdo_VRF_750_800_for_new_rider__hd93yl7kong31_1568720006698_20021.gif',
    '80_motorcycle_application1_1568719028982_6503.gif',
    '55_gia_xe_81_2_1568719088201_7338.gif',
    '24_kawasaki_vulcan_s_abs_2016_moto_saigon_3_1568719119960_7778.gif',
    'Motorheiow5b5smallMotor.jpg',
    'd34z0h_My_first_edit_ever_with_my_first_camera_ever_and_my_first_bike_ever_____camera_just_came_in_today__I_feel_like_I_know_absolutely_nothing_about_photography_and_I_can_t_wait_to_get_better__hfy1fbmik4m31_1568720023653_20196.jpg',
    'd04uf0_My_first_bike___It_s_too_much_bike___2002_Kawasaki_Vulcan_1500_mean_streak__Hypercharger__power_commander__Vance__amp__Hines_pipes___1900_off_of_fb_market__I_have_no_idea_what_I_m_doing_rjs551ccltk31_1568720022054_20178.jpg',
    '29_pic_001_1568719608045_14577.gif',
    '69_xedoisong_Yamaha_MT_03_do_1_MEGC_1568719472292_12693.gif',
    '19_r_636858959614681208_4528_kkk_1568719429934_12092.gif',
    '65_51ew43mofl9ef_1568719232558_9326.gif'

]
for path in missing_paths:
    convert_png(os.path.abspath('.') + '/dataset/raw/motobike/' + path)
