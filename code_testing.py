import numpy as np

from evaluation.client.mifid_demo import MIFID

if __name__ == '__main__':
    evaluation_root = "./evaluation/client/"
    evaluator = MIFID(evaluation_root + "motorbike_classification_inception_net_128_v4_e36.pb",
                      evaluation_root + "public_feature.npz")
    imgs = np.random.randn(1000, 128, 128, 3)
    evaluator.compute_mifid(imgs)
