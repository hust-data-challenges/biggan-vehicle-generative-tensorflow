import math
from functools import partial

import albumentations as A
import cv2
import numpy as np
import torch
import torchvision.transforms as T
from PIL import Image
from albumentations.pytorch import ToTensor
from numba import jit
from torch.utils.data import DataLoader
from torchvision import datasets
from tqdm import tqdm

from utils import get_label_map, PIL_read

IMG_SIZE = 64
IMG_SIZE_2 = IMG_SIZE * 2
IMG_EXTENSIONS = ('.jpg', '.jpeg', '.png', '.ppm', '.bmp', '.pgm', '.tif', '.tiff', '.webp')


class MyImg:
    def __init__(self, img, tfm):
        self.px = np.array(img)
        self.tfm = tfm

    @property
    def size(self):
        h, w, _ = self.px.shape
        return min(w, h)


def pad(img, padding_mode='reflect'):
    p = math.ceil((max(img.size) - min(img.size)) / 2)
    p_horr = p if img.width < img.height else 0
    p_vert = p if img.height < img.width else 0
    img = T.Pad((p_horr, p_vert), padding_mode=padding_mode)(img)
    if img.width != img.height:
        s = min(img.size)
        img = img.crop((0, 0, s, s))
    return img


def take_top(img):
    size = min(img.size)
    bbox = (0, 0, size, size)
    return img.crop(bbox)


def take_diagonal(img):
    w, h = img.size
    size = min(w, h)
    bbox_l = (0, 0, size, size)
    bbox_r = (w - size, h - size, w, h)
    return [img.crop(bbox_l), img.crop(bbox_r)]


resize = T.Resize(IMG_SIZE, interpolation=Image.LANCZOS)
resize2x = T.Resize(IMG_SIZE_2, interpolation=Image.LANCZOS)

center_crop = T.Compose([resize, T.CenterCrop(IMG_SIZE)])
center_crop2x = T.Compose([resize2x, T.CenterCrop(IMG_SIZE_2)])

top_crop = T.Compose([T.Lambda(take_top), resize])
top_crop2x = T.Compose([T.Lambda(take_top), resize2x])

two_crops = T.Compose([resize, T.Lambda(take_diagonal)])
two_crops2x = T.Compose([resize2x, T.Lambda(take_diagonal)])

pad_only = T.Compose([T.Lambda(pad), resize])
pad_only2x = T.Compose([T.Lambda(pad), resize2x])


@jit(nopython=True)
def calc_one_axis(clow, chigh, pad, cmax):
    clow = max(0, clow - pad)
    chigh = min(cmax, chigh + pad)
    return clow, chigh, chigh - clow


def calc_bbox(obj, img_w, img_h, zoom=0.0, try_square=True):
    xmin = int(obj[0])
    ymin = int(obj[1])
    xmax = int(obj[2])
    ymax = int(obj[3])

    # occasionally i get bboxes which exceed img size
    xmin, xmax, obj_w = calc_one_axis(xmin, xmax, 0, img_w)
    ymin, ymax, obj_h = calc_one_axis(ymin, ymax, 0, img_h)

    if zoom != 0.0:
        pad_w = obj_w * zoom / 2
        pad_h = obj_h * zoom / 2
        xmin, xmax, obj_w = calc_one_axis(xmin, xmax, pad_w, img_w)
        ymin, ymax, obj_h = calc_one_axis(ymin, ymax, pad_h, img_h)

    if try_square:
        # try pad both sides equaly
        if obj_w > obj_h:
            pad = (obj_w - obj_h) / 2
            ymin, ymax, obj_h = calc_one_axis(ymin, ymax, pad, img_h)
        elif obj_h > obj_w:
            pad = (obj_h - obj_w) / 2
            xmin, xmax, obj_w = calc_one_axis(xmin, xmax, pad, img_w)

        # if it's still not square, try pad where possible
        if obj_w > obj_h:
            pad = obj_w - obj_h
            ymin, ymax, obj_h = calc_one_axis(ymin, ymax, pad, img_h)
        elif obj_h > obj_w:
            pad = obj_h - obj_w
            xmin, xmax, obj_w = calc_one_axis(xmin, xmax, pad, img_w)

    return int(xmin), int(ymin), int(xmax), int(ymax)


@jit(nopython=True)
def bb2wh(bbox):
    width = bbox[2] - bbox[0]
    height = bbox[3] - bbox[1]
    return width, height


def make_x2res(img, bbox):
    if min(bb2wh(bbox)) < IMG_SIZE_2:
        return
    ar = img.width / img.height
    if ar == 1.0:
        tfm_img = resize2x(img)
    elif 1.0 < ar < 1.15:
        tfm_img = center_crop2x(img)
    elif 1.15 < ar < 1.25:
        tfm_img = pad_only2x(img)
    elif 1.25 < ar < 1.5:
        tfm_img = two_crops2x(img)
    elif 1.0 < 1 / ar < 1.6:
        tfm_img = top_crop2x(img)
    else:
        tfm_img = None
    return tfm_img


def add_sample(samples, label, tfm, imgs, labels):
    if not samples:
        return
    elif isinstance(samples, Image.Image):
        imgs.append(MyImg(samples, tfm))
        labels.append(label)
    elif isinstance(samples, list):
        imgs.extend([MyImg(s, tfm) for s in samples])
        labels.extend([label] * len(samples))
    else:
        assert False


def is_valid_file(x):
    return datasets.folder.has_file_allowed_extension(x, IMG_EXTENSIONS)


class VehicleDataSet(datasets.vision.VisionDataset):
    def __init__(self, root, label_root, transforms, max_samples=None):
        super().__init__(root, transform=None)
        assert isinstance(transforms, list) and len(transforms) == 3
        self.transforms = transforms
        self.max_samples = max_samples
        self.classes = {}

        imgs, labels = self._load_subfolders_images(self.root, label_root)
        assert len(imgs) == len(labels)
        if len(imgs) == 0:
            raise RuntimeError(f'Found 0 files in subfolders of: {self.root}')
        self.imgs = imgs
        self.labels = labels

    def _create_or_get_class(self, name):
        try:
            label = self.classes[name]
        except KeyError:
            label = len(self.classes)
            self.classes[name] = label
        return label

    def _load_subfolders_images(self, root, label_root):
        light_zoom, medium_zoom = 0.08, 0.12
        n_pad, n_center, n_top, n_2crops, n_skip, n_dup, n_noop = 0, 0, 0, 0, 0, 0, 0
        imgs, labels, paths = [], [], []

        add_sample_ = partial(add_sample, imgs=imgs, labels=labels)
        label_mapper = get_label_map(label_root)

        for path in label_mapper:
            paths.append(path)

        if self.max_samples:
            paths = paths[:self.max_samples]

        for path in tqdm(paths):
            if not is_valid_file(path):
                continue
            img = PIL_read(path)
            annotation = label_mapper[path]
            for anno_bbox in annotation:
                prev_bbox, tfm_imgs = None, None

                bbox = calc_bbox(anno_bbox, img_w=img.width, img_h=img.height, zoom=light_zoom)
                obj_img = img.crop(bbox)
                add_sample_(make_x2res(obj_img, bbox), 0, 2)

                bbox = calc_bbox(anno_bbox, img_w=img.width, img_h=img.height)
                if min(bb2wh(bbox)) < IMG_SIZE:
                    # don't want pixel mess in gen imgs
                    n_skip += 1
                    continue
                obj_img = img.crop(bbox)
                ar = obj_img.width / obj_img.height
                if ar == 1.0:
                    tfm_imgs = [resize(obj_img)]
                    n_noop += 1
                elif 1.0 < ar < 1.3:
                    tfm_imgs = [center_crop(obj_img), pad_only(obj_img)]
                    n_center += 1
                    n_pad += 1
                elif 1.3 <= ar < 1.5:
                    tfm_imgs = two_crops(obj_img) + [pad_only(obj_img)]
                    n_2crops += 2
                    n_pad += 1
                elif 1.5 <= ar < 2.0:
                    tfm_imgs = two_crops(obj_img)
                    n_2crops += 2
                elif 1.0 < 1 / ar < 1.5:
                    tfm_imgs = [top_crop(obj_img), pad_only(obj_img)]
                    n_top += 1
                    n_pad += 1
                elif 1.5 <= 1 / ar < 1.8:
                    tfm_imgs = [top_crop(obj_img)]
                    n_top += 1
                else:
                    tfm_imgs = None
                    n_skip += 1
                add_sample_(tfm_imgs, 0, 0)
                add_sample_(make_x2res(obj_img, bbox), 0, 1)
                prev_bbox = bbox

                bbox = calc_bbox(anno_bbox, img_w=img.width, img_h=img.height, zoom=medium_zoom, try_square=False)
                if bbox == prev_bbox:
                    n_dup += 1
                    continue
                if min(bb2wh(bbox)) < IMG_SIZE_2: continue
                obj_img = img.crop(bbox)
                ar = obj_img.width / obj_img.height
                if 1.3 < ar < 1.5:
                    tfm_imgs = two_crops(obj_img)
                    n_2crops += 2
                elif 1.05 < 1 / ar < 1.6:  # maybe tall
                    tfm_imgs = top_crop(obj_img)
                    n_top += 1
                else:
                    continue
                add_sample_(tfm_imgs, 0, 0)
                add_sample_(make_x2res(obj_img, bbox), 0, 1)
                prev_bbox = bbox

        n_x1, n_x2 = 0, 0
        for i, img in enumerate(imgs):
            if img.size == IMG_SIZE:
                n_x1 += 1
            else:
                n_x2 += 1

        print(f'Loaded 64x64 {n_x1} images\n'
              f'Loaded 128x128 {n_x2} images\n')
        print(f'Pad only: {n_pad}\nCrop center: {n_center}\n'
              f'Crop top: {n_top}\nCrop 2 times: {n_2crops}\n'
              f'Take as-is: {n_noop}\nSkip: {n_skip}\nSame bbox: {n_dup}')
        return imgs, labels

    def __getitem__(self, index):
        img = self.imgs[index]
        label = self.labels[index]
        tfms = self.transforms[img.tfm]
        img = tfms(image=img.px)['image']
        # if self.target_transform:
        #     label = self.target_transform(label)
        return img, label

    def __len__(self):
        return len(self.imgs)


def create_runtime_tfms():
    mean, std = [0.5] * 3, [0.5] * 3
    resize_to_64 = A.SmallestMaxSize(IMG_SIZE, interpolation=cv2.INTER_AREA)
    out = [A.HorizontalFlip(p=0.5), A.Normalize(mean=mean, std=std), ToTensor()]

    rand_crop = A.Compose([
        A.SmallestMaxSize(IMG_SIZE + 8, interpolation=cv2.INTER_AREA),
        A.RandomCrop(IMG_SIZE, IMG_SIZE)
    ])

    affine_1 = A.ShiftScaleRotate(
        shift_limit=0, scale_limit=0.1, rotate_limit=8,
        interpolation=cv2.INTER_CUBIC,
        border_mode=cv2.BORDER_REFLECT_101, p=1.0)
    affine_1 = A.Compose([affine_1, resize_to_64])

    affine_2 = A.ShiftScaleRotate(
        shift_limit=0.06, scale_limit=(-0.06, 0.18), rotate_limit=6,
        interpolation=cv2.INTER_CUBIC,
        border_mode=cv2.BORDER_REFLECT_101, p=1.0)
    affine_2 = A.Compose([affine_2, resize_to_64])

    tfm_0 = A.Compose(out)
    tfm_1 = A.Compose([A.OneOrOther(affine_1, rand_crop, p=1.0), *out])
    tfm_2 = A.Compose([affine_2, *out])
    return [tfm_0, tfm_1, tfm_2]


# multi-epoch Dataset sampler to avoid memory leakage and enable resumption of
# training from the same sample regardless of if we stop mid-epoch
class MultiEpochSampler(torch.utils.data.Sampler):
    r"""Samples elements randomly over multiple epochs

    Arguments:
        data_source (Dataset): dataset to sample from
        num_epochs (int) : Number of times to loop over the dataset
        start_itr (int) : which iteration to begin from
    """

    def __init__(self, data_source, num_epochs, start_itr=0, batch_size=128):
        self.data_source = data_source
        self.num_samples = len(self.data_source)
        self.num_epochs = num_epochs
        self.start_itr = start_itr
        self.batch_size = batch_size

        if not isinstance(self.num_samples, int) or self.num_samples <= 0:
            raise ValueError("num_samples should be a positive integeral "
                             "value, but got num_samples={}".format(self.num_samples))

    def __iter__(self):
        n = len(self.data_source)
        # Determine number of epochs
        num_epochs = int(np.ceil((n * self.num_epochs
                                  - (self.start_itr * self.batch_size)) / float(n)))
        # Sample all the indices, and then grab the last num_epochs index sets;
        # This ensures if we're starting at epoch 4, we're still grabbing epoch 4's
        # indices
        out = [torch.randperm(n) for epoch in range(self.num_epochs)][-num_epochs:]
        # Ignore the first start_itr % n indices of the first epoch
        out[0] = out[0][(self.start_itr * self.batch_size % n):]
        # if self.replacement:
        # return iter(torch.randint(high=n, size=(self.num_samples,), dtype=torch.int64).tolist())
        # return iter(.tolist())
        output = torch.cat(out).tolist()
        print('Length dataset output is %d' % len(output))
        return iter(output)

    def __len__(self):
        return len(self.data_source) * self.num_epochs - self.start_itr * self.batch_size


def get_data_loaders(data_root=None, label_root=None, batch_size=32,
                     augment=1, num_epochs=500,
                     num_workers=2, shuffle=True, pin_memory=True, drop_last=True,
                     use_multiepoch_sampler=None, start_itr=None):
    print('Using dataset root location %s' % data_root)

    transform = None
    if augment == 1:
        transform = create_runtime_tfms()

    train_set = VehicleDataSet(data_root, label_root, transform)
    # Prepare loader; the loaders list is for forward compatibility with
    # using validation / test splits.
    loaders = []
    if use_multiepoch_sampler:
        print('Using multiepoch sampler from start_itr %d...' % start_itr)
        loader_kwargs = {'num_workers': num_workers, 'pin_memory': pin_memory}
        sampler = MultiEpochSampler(train_set, num_epochs, start_itr,
                                    batch_size)
        train_loader = DataLoader(train_set, batch_size=batch_size,
                                  sampler=sampler, **loader_kwargs)
    else:
        loader_kwargs = {'num_workers': num_workers, 'pin_memory': pin_memory,
                         'drop_last': drop_last}  # Default, drop last incomplete batch
        train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=shuffle, **loader_kwargs)
    loaders.append(train_loader)
    return loaders
